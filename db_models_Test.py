#!/usr/local/bin/python2.7
# encoding: utf-8
"""
:synopsis:   Unit Testing for db_models module.

It defines the following classes and functions:
    :class:`DBInitializationTest`
    :class:`ProteinTest`

:created:    2014-11-13

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.8
:updated:    2015-03-09
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division
from __future__ import print_function

from pprint import pprint as print
import random

import unittest

import db_models as dbm


#===============================================================================
# Global variables
#===============================================================================
PHOSPHORYLATION = {'id': 21, 'name': 'Phospho', 'base_ascore_threshold': 19}
ACETYLATION = {'id': 1, 'name': 'Acetyl'}
UBIQUITINATION = {'id': 121, 'name': 'GG'}


#===============================================================================
# Class definitions
#===============================================================================
class DBTestBaseMixin(object):

    @staticmethod
    def memory_db_w_tables():
        memory_db = dbm.DataBase('sqlite:///:memory:')
        memory_db.create_tables()
        return memory_db


class DataBaseTest(unittest.TestCase, DBTestBaseMixin):
    @classmethod
    def setUpClass(cls):
        pass

    def setUp(self):
        self.test_db = dbm.DataBase('sqlite:///:memory:')

    def test_db_creation(self):
        test_db = dbm.DataBase('sqlite:///:memory:')
        self.assertIsInstance(test_db, dbm.DataBase)
        self.assertFalse( test_db.db_engine.table_names() )

    def test_create_tables(self):
        test_db = self.test_db
        db_tables = test_db.create_tables()
        self.assertItemsEqual( db_tables.keys(),
                               test_db.db_engine.table_names() )

    def test_session(self):
        from sqlalchemy.orm import Session
        test_db = self.test_db
        with test_db.session() as session:
            self.assertIsInstance(session, Session)


class ProteinTest(unittest.TestCase, DBTestBaseMixin):
    @classmethod
    def setUpClass(cls):
        cls.db = cls.memory_db_w_tables()
        cls.basicprot = dbm.Protein(ac='P00001', name='TestProt1', seq='A'*150)

    def setUp(self):
        random.seed()

    def tearDown(self):
        pass

    def test_init(self):
        num = random.randint(1,10)
        ac = 'P0000{0}'.format(num)
        name = 'Prot{0}'.format(num)
        prot = dbm.Protein(ac=ac, name=name)
        self.assertIsInstance(prot, dbm.Protein)
        self.assertEqual(prot.ac, ac)
        self.assertEqual(prot.name, name)
        self.assertIsInstance(prot.modifications, set)
        self.assertIsInstance(prot.pos2modifications, dict)
        self.assertIsInstance(prot._hd_ptm_regions, dict)

    def test_getittem(self):
        protein = dbm.Protein(ac='P00002', name='TestProt2', seq='1234567890')
        bio_pos = random.randint(1, 9)
        self.assertIsInstance(protein[bio_pos], tuple)
        self.assertIsInstance(protein[bio_pos][1], set)
        self.assertEqual( protein[1][0], '1' )
        self.assertEqual( protein[bio_pos][0], str(bio_pos) )
        self.assertEqual( protein[-1][0], '0' )
        #
        self.assertRaises(IndexError, protein.__getitem__, 0)
        #
        prot_reg = protein[5:10]
        self.assertIsInstance(prot_reg, dbm.ProtRegion)
        self.assertEqual(prot_reg.protein, protein)
        self.assertEqual(prot_reg.seq, '567890')
        self.assertEqual(prot_reg[1][0], '5')
        self.assertEqual(prot_reg[5][0], '9')
        self.assertEqual(prot_reg[-1][0], '0')
        self.assertEqual(prot_reg[-5][0], '6')

    def test_add_mod_type(self):
        protein = self.basicprot
        position = random.randint(0,150-1)
        mod_type = random.randint(1, 50)
        #
        added_mod = protein.add_mod_type(position, mod_type)
        self.assertEqual(added_mod.protein, protein)
        self.assertEqual(added_mod.position, position)
        self.assertIs(added_mod.mod_type.id, mod_type)
        self.assertEqual(added_mod.aa, protein.seq[position])
        self.assertIn(added_mod, protein.pos2modifications[position])

    def test_add_modification(self):
        protein = self.basicprot
        position = random.randint(0, 150-1)
        #
        modifications = ( dbm.ProtModification(position=position),
                          dbm.ProtModification() )
        added_mods = ( protein.add_modification(modifications[0]),
                       protein.add_modification(modifications[1], position) )
        #
        for modification, added_mod in zip(modifications, added_mods):
            self.assertIs(modification, added_mod)
            self.assertEqual(modification.position, position)
            self.assertEqual(modification.protein, protein)
            self.assertIn(modification, protein.pos2modifications[position])

    def test_digest(self):
        protein = dbm.Protein(ac='P00003', name='TestProt3', seq='AAARAAAAAAKARPA')
        for missing_cleavages, min_length, expected in ( (0, 1, 3), (2, 5, 4) ):
            digestion_peps = protein.digest_with(dbm.trypsin, missing_cleavages, min_length)
            self.assertIsInstance(digestion_peps, set)
            self.assertTrue(len(digestion_peps) == expected)
            for digestion_pep in digestion_peps:
                self.assertIsInstance(digestion_pep, dbm.DigestionPeptide)
                self.assertIn(digestion_pep.seq, protein.seq)
                self.assertTrue(digestion_pep.length >= min_length)
                print(digestion_pep)


class DigestionPeptideTest(unittest.TestCase, DBTestBaseMixin):
    @classmethod
    def setUpClass(cls):
        cls.db = cls.memory_db_w_tables()
        cls.basicprot = dbm.Protein(ac='P00001', name='TestProt1', seq='AAARASAAAAKARPA')
        cls.phosphorilation = dbm.ModificationType(**PHOSPHORYLATION)
        cls.acetylation = dbm.ModificationType(**ACETYLATION)
        cls.ubiquitination = dbm.ModificationType(**UBIQUITINATION)
        cls.basicprot.add_mod_type(6, cls.phosphorilation)
        cls.basicprot.add_mod_type(11, cls.acetylation)
        cls.basicprot.add_mod_type(11, cls.ubiquitination)

    def setUp(self):
        self.test_digpep = dbm.DigestionPeptide(protein=self.basicprot,
                                                prot_start=5,
                                                prot_end=11)

    def tearDown(self):
        pass

    def test_init(self):
        self.assertIsInstance(self.test_digpep, dbm.DigestionPeptide)
        self.assertIs(self.test_digpep.protein, self.basicprot)
        self.assertEqual(self.test_digpep.seq, 'ASAAAAK')
        self.assertTrue(self.test_digpep.consensus)
        self.assertEqual(len(self.test_digpep.modifications), 3)

    def test_seq_w_mods(self):
        self.assertEqual(self.test_digpep.seq_w_mods, 'AS[21]AAAAK[1, 121]')
        self.test_digpep.add_mod_type(2, self.ubiquitination)
        self.assertEqual(self.test_digpep.seq_w_mods, 'AS[21]AAAAK[1, 121]')
        self.test_digpep.refresh_seq_w_mods()
        self.assertEqual(self.test_digpep.seq_w_mods, 'AS[21, 121]AAAAK[1, 121]')
    
    def test_seq_w_mods2seq(self):
        self.assertEqual(self.test_digpep.seq_w_mods2seq(self.test_digpep.seq_w_mods), 
                         self.test_digpep.seq)
        self.assertEqual(self.test_digpep.seq_w_mods2seq('A[21]BB[23, 1]CCC'), 
                         'ABBCCC')
        self.assertEqual(self.test_digpep.seq_w_mods2seq('AS[1, 21, 121]AA[21]AAK[1, 121]'), 
                         'ASAAAAK')
        
    def test_pos2modifications(self):
        pos2modifications = self.test_digpep.pos2modifications
        modifications = self.test_digpep.modifications
        self.assertIsInstance(pos2modifications, dict)
        self.assertItemsEqual( pos2modifications.keys(),
                               {mod.position - self.test_digpep.prot_start + 1
                                for mod in modifications} )
        all_posmodifications = list()
        for mods in pos2modifications.values():
            self.assertIsInstance(mods, set)
            all_posmodifications.extend(mods)
        self.assertItemsEqual(modifications, all_posmodifications)

    def test_prot_pos2modifications(self):
        prot_pos2modifications = self.test_digpep.prot_pos2modifications
        modifications = self.test_digpep.modifications
        self.assertIsInstance(prot_pos2modifications, dict)
        self.assertItemsEqual( prot_pos2modifications.keys(),
                               {mod.position for mod in modifications} )
        all_prot_posmodifications = list()
        for mods in prot_pos2modifications.values():
            self.assertIsInstance(mods, set)
            all_prot_posmodifications.extend(mods)
        self.assertItemsEqual(modifications, all_prot_posmodifications)

    def test_pos_modtype2modifications(self):
        pos_modtype2modifications = self.test_digpep.pos_modtype2modifications
        self.assertIsInstance(pos_modtype2modifications, dict)
        self.assertItemsEqual( pos_modtype2modifications.keys(),
                               ( (2, self.phosphorilation),
                                 (7, self.acetylation),
                                 (7, self.ubiquitination) ) )
        modifications = pos_modtype2modifications.values()
        self.assertEqual(len(modifications), 3)
        self.assertIsInstance(modifications[0], set)
        for mod in self.test_digpep.modifications:
            pos_modtype = (mod.position - self.test_digpep.prot_start + 1,
                           mod.mod_type)
            self.assertIn( mod,
                           pos_modtype2modifications[pos_modtype] )

    def test_make_combinatorial_peptides(self):
        combinatorial_peptides = self.test_digpep.make_combinatorial_peptides()
        self.assertIsInstance(combinatorial_peptides, list)
        self.assertItemsEqual(self.test_digpep.combinatorial_peptides,
                              combinatorial_peptides)
        self.assertIs(combinatorial_peptides[0], self.test_digpep)
        self.assertEqual(len(combinatorial_peptides[1].modifications), 0)
        self.assertEqual(len(combinatorial_peptides), 3+3+2)
        seqs_w_mods = set()
        for dig_pep in combinatorial_peptides:
            print(dig_pep.seq_w_mods)
            seqs_w_mods.add(dig_pep.seq_w_mods)
            self.assertTrue(self.test_digpep.modifications.issuperset(dig_pep.modifications))
        self.assertEqual( len(seqs_w_mods), len(combinatorial_peptides) )


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == "__main__":
    unittest.main()

