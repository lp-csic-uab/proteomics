# -*- coding: utf-8 -*-
"""
:synopsis: Common Basic Functions for Proteomics (DEPRECATED version).

:caution:    DEPRECATED version. Please update to use ``pepprot_func.py``.

:created:    2013/06/26

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.3
:updated:    2014-10-16
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division

import csv
import re
from collections import defaultdict
import numpy as np

from general.basic_func import deep_getattr
from commons import mass

from unimod_globals import UNIMOD
from basic_class import *
import db_models as dbm

#===============================================================================
# Functions declarations
#===============================================================================
def new_protein_from_fasta(seq_header, seq_buffer):
    new_prot = dbm.Protein()
    new_prot.ac = seq_header.split('|')[1]
    new_prot.name = seq_header
    new_prot.seq = ''.join(seq_buffer)
    return new_prot


def load_fasta_data(fasta_filen):
    fasta_prots = dict()
    #
    with open(fasta_filen, 'r') as io_file:
        seq_header = ''
        seq_buffer = list()
        curr_prot = None
        for row in io_file:
            row = row.strip()
            if row[0] == '>':
                if seq_header and seq_buffer:
                    curr_prot = new_protein_from_fasta(seq_header, seq_buffer)
                    fasta_prots[curr_prot.ac] = curr_prot
                seq_buffer = list()
                seq_header = row
            else:
                seq_buffer.append(row)
        if seq_header and seq_buffer:
            curr_prot = new_protein_from_fasta(seq_header, seq_buffer)
            fasta_prots[curr_prot.ac] = curr_prot
    #
    return fasta_prots


def load_fasta_data_old(filen, fasta_type='OMSSA'):
    fasta_prots = dict()
    
    with open(filen, 'r') as io_file:
        seq_header = ''
        seq_buffer = list()
        curr_prot = None
        for row in io_file:
            row = row.strip()
            if row[0] == '>':
                if row is not seq_header:
                    if seq_header and seq_buffer:
                        curr_prot = dbm.Protein({'Defline': seq_header}, 
                                                trans_tbl_sel=fasta_type)
                        curr_prot.seq = ''.join(seq_buffer)
                        fasta_prots[curr_prot.ac] = curr_prot
                    seq_buffer = list()
                    seq_header = row
                else:
                    seq_buffer = list()
            else:
                seq_buffer.append(row)
        if seq_header and seq_buffer:
            curr_prot = dbm.Protein({'Defline': seq_header}, 
                                    trans_tbl_sel=fasta_type)
            curr_prot.seq = ''.join(seq_buffer)
            fasta_prots[curr_prot.ac] = curr_prot
        
    return fasta_prots


def proteins_by_ac(proteins):
    return {prot.ac: prot for prot in proteins}


def proteins_by_pep(proteins):
    proteins_pep = defaultdict(list)
    for protein in proteins:
        for data_record in proteins.data.all():
            proteins_pep[data_record.peptide].append(protein)
    return proteins_pep


def peptides_by_attr(data_records, attr, *attrs):
    """
    Group data_records (iterable of Data objects) by one (attr) or more
    (attrs) of its attributes/properties values and summarize some
    information about them.
    """
    # Skeleton of the dictionary used for grouping:
    peptides = defaultdict(lambda: {'n_scans': 0, 
                                    'quantitative': False,
                                    'data_records': list()})
    if attrs:
        attrs = (attr,) + attrs
    # Iterate over record Data instances in data_records (a QuerySet with
    # experimental peptide data), and group by pep_orig in peptides dict:
    for record in data_records:
        if not attrs:
            gid = deep_getattr(record, attr)
        else:
            multiple_gid = list()
            for an_attrr in attrs:
                multiple_gid.append( deep_getattr(record, an_attrr) )
            gid = tuple(multiple_gid)
        # Record is from a new peptide:
        if gid not in peptides:
            peptides[gid]['pid'] = gid
            peptides[gid]['pep_orig'] = record.pep_orig
            peptides[gid]['peptide'] = record.peptide.upper()
            peptides[gid]['calc_mass'] = pep_mod_mass(peptides[gid])
            l_prots = record.proteins_w_decoys.all().values_list('ac',      #TODO: get proteins from FASTA_FILEN
                                                                 flat=True) #
            peptides[gid]['l_prots_w_decoys'] = set(l_prots)                #
            peptides[gid]['psites'] = record.get_psites()
            peptides[gid]['phospho'] = record.phospho
            peptides[gid]['max_ascores'] = record.get_ascores()
        # Record is from a previously collected phospho-peptide:
        else:
            l_prots = record.proteins_w_decoys.all().values_list('ac',      #TODO: get proteins from FASTA_FILEN
                                                                 flat=True) #
            peptides[gid]['l_prots_w_decoys'].update(l_prots)               #
            if peptides[gid]['phospho']:
                grouped_ascores = zip(peptides[gid]['max_ascores'], 
                                      record.get_ascores())
                peptides[gid]['max_ascores'] = map(max, grouped_ascores)
        # Always, afterwards/finally, do:
        try:
            if not peptides[gid]['quantitative'] and record.qdata \
               and record.dataquant:
                peptides[gid]['quantitative'] = True
        except ObjectDoesNotExist:
            pass
        peptides[gid]['n_scans'] += 1
        peptides[gid]['data_records'].append(record)
    #
    return peptides


def peptides_by_pep_orig(data_records):
    """
    Group data_records (iterable of Data objects) by original peptide
    attribute ('pep_orig') and summarize some information about them.
    """
    return peptides_by_attr(data_records, 'pep_orig')


def peptides_by_ac(peptides_dict):
    """
    Group peptides_dict (dict of peptide dicts) by proteins AC (ac).
    """
    # Skeleton of the dictionary used for grouping:
    ac2peps = defaultdict(dict)
    # Iterate over peptide dicts in peptides_dict, and group by
    # peptide['l_prots_w_decoys'] ac into ac2peps dict:
    for pid, peptide in peptides_dict.items():
        for ac in peptide['l_prots_w_decoys']:
            ac2peps[ac][pid] = peptide
    return ac2peps


def find_mods(pep_orig, unimods=None, fixed_mods=None):
    """
    Locates UNIMOD modifications, and returns a list of dictionaries
    containing location and UNIMOD information
    """
    if not unimods:
        unimods = UNIMOD
    if not fixed_mods: #TODO: make it more general not so case-specific
        # Find carbamidomethylations X-NOTE: default static modification:
        fixed_mods = {'C': UNIMOD[4]}
    pos = 0
    mods = list()
    aas_and_mods = re.split('[\\)\\(, ]', pep_orig)
    for aas_or_mods in aas_and_mods: #Mods numbers (unimods)
        if aas_or_mods.isdigit():
            mod = unimods[int(aas_or_mods)].copy()
            mod['pos'] = pos
            mods.append(mod)
        else: #AAs letters (fixed_mods)
            for residue, unimod in fixed_mods.items():
                sub_pos = 0
                c_pos = aas_or_mods.find(residue, sub_pos)
                while c_pos > -1:
                    sub_pos = c_pos + 1
                    mod = unimod.copy()
                    mod['pos'] = pos + sub_pos
                    mods.append(mod)
                    c_pos = aas_or_mods.find(residue, sub_pos)
            pos += len(aas_or_mods)
    return mods


def old_regex_format(rs_search, regex_replace):
    """
    #DEPRECATED: See new method regex_format()
    Formating of rs_search string according to regex_replace dictionary
    """
    rl_search = list()
    rkeys = regex_replace.keys()
    for each in list(rs_search):
        if each in rkeys:
            each = regex_replace[each]
        rl_search.append(each)
    return ''.join(rl_search)


def regex_format(original, replace_tbl=None):
    """
    Formating of original string according to a replace_tbl dictionary 
    ( {each: regex} ).
    Ex.: AAABAAXAAI -> AAA[BND]AA.AA[IJ]
    """
    if not replace_tbl: #Default replace table for amino-acids
        replace_tbl = {'B': '[BND]',
                       'Z': '[ZEQ]',
                       'J': '[JIL]',
                       'X': '[.]',
                       'I': '[IJ]',
                       'L': '[LJ]',
                       'Other': '{0}',
                       #'Global': '{0}', #Return the resulting regex, without using a lookahead assertion to accelerate searches
                       'Global': '(?={0})', #Add a lookahead assertion (allows overlaping matches, but those matches are void unless a capture group is specified in the pattern such as '(?=({0}))' : X-NOTE: http://stackoverflow.com/a/11430936) and return the regex
                       }
    regex_search = list()
    rkeys = replace_tbl.keys()
    for each in list(original):
        if each in rkeys:
            each = replace_tbl[each]
        else:
            each = replace_tbl['Other'].format(each)
        regex_search.append(each)
    return replace_tbl['Global'].format(''.join(regex_search))


def regex_format_old(rs_search, regex_replace):
    '''
    Protection against regular expressions and formating of rs_search string
    according to REGEX_REPLACE dictionary
    '''
    rl_search = list()
    rkeys = regex_replace.keys()
    for each in list(rs_search):
        each = re.escape(each) #Protection against regular expressions
        if each in rkeys:
            each = regex_replace[each]
        rl_search.append(each)
    return ''.join(rl_search)


def re_search_pep_in_prots(pep, prots):
    found_prots = list()
    replace_tbl = {'B': '[ND]',
                   'Z': '[EQ]',
                   'J': '[IL]',
                   'X': '.'}
    pep = regex_format(pep.upper(), replace_tbl)
    
    for prot in prots:
        pep_pos = list()
        for match in re.finditer(pep, prot.seq):
            pep_pos.append((match.start(), match.end() - 1))
        c_prot = prot.shallow_copy()
        c_prot.pep_pos = pep_pos
        found_prots.append(c_prot)
    
    return found_prots


def re_search_pep_in_prots_old(pep, prots):
    prots_w_peppos = list()
    pep = regex_format(pep.upper())
    #
    for prot in prots:
        pep_pos = list()
        for match in re.finditer(pep, prot.seq):
            pep_pos.append((match.start()+1, match.end()))
        prots_w_peppos.append( (prot, pep_pos) )
    #
    return prots_w_peppos


def protein_coverage(protein, peptides):
    covered_aa_pos = set() #Amino-acids positions covered by peptides
    for peptide in peptides.values():
        pep_prots = [protein]
        prots_w_peppos = re_search_pep_in_prots(peptide['peptide'], pep_prots)
        for _, pep_poss in prots_w_peppos:
            for pep_pos in pep_poss:
                start, end = pep_pos
                covered_aa_pos.add(range(start, end+1))
    # Coverage calculus:
    return len(covered_aa_pos) / len(protein.seq) * 100


def mean_of_peps_sds(data_redords):
    stdvs = defaultdict(list)
    for data_redord in data_redords:
        try:
            time_values = data_redord.dataquant.time_values()
        except ObjectDoesNotExist: #data_record has not dataquant related records (is not quantitative)
            continue
        for time, (reg_v, ratio, stdv) in time_values.items():
            stdvs[time].append(stdv)
    means = dict()
    for time in stdvs.keys():
        means[time] = np.mean(stdvs[time])
    return means


def pep_mod_mass(peptide, unimods=None, fixed_mods=None):
    pep_seq = peptide['peptide']
    pep_mass = mass.get_mass(pep_seq)
    #
    pep_orig = peptide['pep_orig']
    mods = find_mods(pep_orig)
    for mod in mods:
        pep_mass += mod['delta_mass']
    return pep_mass


def get_Psite_seqs_from_Ppeptides(peptides, protein, prot_seq=None, 
                                  extend=7, phospho_sites=None):
    """
    Extend by <extend> amino acids the C- and N-terms of each phospho site
    found in the protein from the peptide sequence in peptides iterable

    Returns a phospho_sites defaultdict: 
      {phospo_site_seq: ( set(ProteinPSiteData), set(PeptidePSiteData) ), ...}
    Allows direct modification of a supplied phospho_sites defaultdict (acts like a ByRef parameter):
      defaultdict( lambda: PSiteSeqData( set(), set() ) )

    Highly Modified from enlarge_sequences() in tools.phoscripts.kinase_motifs
    """
    if not prot_seq:
        prot_seq = protein.seq
    extreme = '-' * extend
    prot_seq = extreme + prot_seq + extreme
    #
    if not isinstance(phospho_sites, defaultdict): #Allows direct modification of a supplied phospho_sites defaultdict
        phospho_sites = defaultdict( lambda: PSiteSeqData( set(), set() ) )
    #
    for pep in peptides:
        pep_seq = pep.peptide
        reges_sequence = regex_format(pep_seq)
        found = False
        for match in re.finditer(reges_sequence, prot_seq):
            found = True
            for pep_psite, pep_ascore in zip(pep.psites, pep.ascores):
                start = match.start() + pep_psite
                tail = prot_seq[ start-extend : start ]
                head = prot_seq[ start+1 : start+1+extend ]
                sec = tail + pep_seq[pep_psite] + head
                phospho_sites[sec].proteins.add( ProteinPSiteData(protein, 
                                                                  protein.ac, 
                                                                  start, 
                                                                  prot_seq[pep_psite]) )
                phospho_sites[sec].peptides.add( PeptidePSiteData(pep, pep_seq, 
                                                                  pep_ascore) )
        #
        if not found:
            print 'not found {0}'.format(pep_seq)
            get_Psite_seqs_from_Ppeptides([pep], protein, pep_seq.upper(), #Mock recursion where prot_seq is subtituted with pep_seq.upper()
                                          extend, phospho_sites)
    #
    return phospho_sites


def get_Psites4prot(protein, phospho_sites=None):
    """
    :params protein: a :class:`lymphos.db_models.Proteins' instance.
    :params phospho_sites: an instance of :class:`defaultdict`
    (``defaultdict( PSite )``) to be modified in place (acts like a ByRef
    parameter).
    
    :returns: a defaultdict of sequence positions to :class:`PSite`
    objects::
      {position_in_seq: PSite(prot_ac, position, p_aa, max_ascore, supporting_peps), ...}
    """
    if not isinstance(phospho_sites, defaultdict): #Allows direct modification of a supplied phospho_sites defaultdict
        phospho_sites = defaultdict(PSite)
    #
    for pep in protein.phosphopeptides:
        pep_seq_regex = regex_format(pep.peptide.upper())
        found = False
        for match in re.finditer(pep_seq_regex, protein.seq):
            found = True
            for pep_psite, pep_ascore in zip(pep.psites, pep.ascores):
                pos = match.start() + pep_psite + 1
                p_site = phospho_sites[pos] #Get the PSite object for in-situ modification
                if not p_site.position:
                    p_site.position = pos
                    p_site.prot_ac = protein.ac
                    p_site.p_aa = protein.seq[pos-1]
                p_site.max_ascore = max(p_site.max_ascore, pep_ascore)
                p_site.supporting_peps.add(pep)
        #
        if not found:
            print 'not found {0}'.format(pep.peptide)
            phospho_sites[0].supporting_peps.add(pep)
    #
    return phospho_sites


def load_data_OMSSA_CSV(filen, fasta_file):
    '''
    Load data from CSV file (filen), store it in dictionaries, and return them as a tuple:
      all_bacteria = {bacteria_id: bateria object}
      all_strains = {strain_id: strain object}
      all_proteins = {protein_ac: protein object}
    '''
    fasta_prots = load_fasta_data(fasta_file)
    
    all_bacteria = {'B. hyodysenteriae': Bacteria(id = 'B. hyodysenteriae', 
                                                  valid_strains = ['INFE1', 'V1', 'LL1'], 
                                                  strains = dict()), 
                    'B. pilosicoli': Bacteria(id= 'B. pilosicoli', 
                                              valid_strains = ['OLA9', 'Vi13'], 
                                              strains = dict()), 
                    }
    all_strains = dict()
    all_proteins = dict()

    with open(filen, 'r') as io_file:
        csvdreader = csv.DictReader(io_file, delimiter=';')
        for csv_dict in csvdreader:
            curr_strain = Strain(csv_dict, trans_tbl_sel = 'OMSSA')
            if all_strains.has_key(curr_strain.id):
                curr_strain = all_strains[curr_strain.id]
            else:
                all_strains[curr_strain.id] = curr_strain
            
            curr_sample = Sample(csv_dict, trans_tbl_sel = 'OMSSA')
            if curr_strain.samples.has_key(curr_sample.id):
                curr_sample = curr_strain.samples[curr_sample.id]
            else:
                curr_strain.samples[curr_sample.id] = curr_sample
            
            #pep = csv_dict['Peptide']
            curr_psm = PSM(csv_dict)
            curr_sample.psms[curr_psm.seq].append(curr_psm)
            for curr_protein in simple_search_pep_in_prots(curr_psm.seq, 
                                                           fasta_prots.values()):
                if all_proteins.has_key(curr_protein.ac):
                    curr_protein = all_proteins[curr_protein.ac]
                else:
                    all_proteins[curr_protein.ac] = curr_protein
                curr_protein.peptides.add(curr_psm.seq)
                curr_protein.strains[curr_strain.id] = curr_strain
                curr_strain.proteins[curr_protein.ac] = curr_protein
                curr_sample.proteins[curr_protein.ac] = curr_protein
                curr_psm.proteins[curr_protein.ac] = curr_protein
        
        for strain in all_strains.values():
            for bacteria in all_bacteria.values():
                if strain.id in bacteria.valid_strains:
                    bacteria.strains[strain.id] = strain
        
    return all_bacteria, all_strains, all_proteins


def load_data_PD_TSV(filens, fasta_file):
    '''
    Load data from CSV file (filen), store it in dictionaries, and return them as a tuple:
      all_bacteria = {bacteria_id: bateria object}
      all_strains = {strain_id: strain object}
      all_proteins = {protein_ac: protein object}
    '''
    fasta_prots = load_fasta_data(fasta_file)
    
    all_bacteria = {'B. hyodysenteriae': Bacteria(id = 'B. hyodysenteriae', 
                                                  valid_strains = ['INFE1', 'V1', 'LL1'], 
                                                  strains = dict()), 
                    'B. pilosicoli': Bacteria(id= 'B. pilosicoli', 
                                              valid_strains = ['OLA9', 'Vi13'], 
                                              strains = dict()), 
                    }
    all_strains = dict()
    all_proteins = dict()
    
    for filen in filens:
        curr_strain = Strain({'File': filen}, trans_tbl_sel = 'PD')
        if all_strains.has_key(curr_strain.id):
            curr_strain = all_strains[curr_strain.id]
        else:
            all_strains[curr_strain.id] = curr_strain
        
        curr_sample = Sample({'File': filen}, trans_tbl_sel = 'PD')
        if curr_strain.samples.has_key(curr_sample.id):
            curr_sample = curr_strain.samples[curr_sample.id]
        else:
            curr_strain.samples[curr_sample.id] = curr_sample
        
        with open(filen, 'r') as io_file:
            csvdreader = csv.DictReader(io_file, delimiter='\t')
            for csv_dict in csvdreader:
                curr_protein = dbm.Protein(csv_dict, trans_tbl_sel='PD') 
                if all_proteins.has_key(curr_protein.ac):
                    curr_protein = all_proteins[curr_protein.ac]
                else:
                    curr_protein.seq = getattr(fasta_prots.get(curr_protein.ac, None), 'seq', '')
                    all_proteins[curr_protein.ac] = curr_protein
                curr_protein.strains[curr_strain.id] = curr_strain
                curr_strain.proteins[curr_protein.ac] = curr_protein
                curr_sample.proteins[curr_protein.ac] = curr_protein
        
    for strain in all_strains.values():
        for bacteria in all_bacteria.values():
            if strain.id in bacteria.valid_strains:
                bacteria.strains[strain.id] = strain
    
    return all_bacteria, all_strains, all_proteins


def save_prot_data(filen, prot_acs, all_proteins, data_headers = None):
    '''
    Generic function to save protein data from a iterable containing protein ACs into a filen CSV file:
    From those proteins, also put apart the uncharacterized ones.
    '''
    if not data_headers:
        data_headers = ['ac','name','gene','organism','evidence','database','revised']
    
    with open(filen + '.csv', 'wb') as io_file:
        with open(filen + '_uncharacterized.csv', 'wb') as io_file_unchard:
            csvdwriter = csv.DictWriter(io_file, data_headers, extrasaction='ignore', delimiter=';')
            csvdwriter_unchard = csv.DictWriter(io_file_unchard, data_headers, extrasaction='ignore', delimiter=';')
            csvdwriter.writer.writerow(data_headers)
            csvdwriter_unchard.writer.writerow(data_headers)
            curr_csvdwriter = None
            for prot_ac in sorted(prot_acs):
                if 'uncharacterized protein' not in all_proteins[prot_ac].name:
                    curr_csvdwriter = csvdwriter
                else:
                    curr_csvdwriter = csvdwriter_unchard
                curr_csvdwriter.writerow(all_proteins[prot_ac].__dict__)
    return


def simple_search_pep_in_prots(pep, prots):
    found_prots = list()
    
    for prot in prots:
        if pep in prot.seq:
            found_prots.append(prot.shallow_copy())
    
    return found_prots

