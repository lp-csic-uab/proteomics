---

**WARNING!**: This is the *Old* source-code repository for Proteomics shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/proteomics_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/proteomics_code/**  

---  
  
  
# Proteomics shared Package

Python 2.7.x package for proeomics, and peptide and protein manipulation.  


---

**WARNING!**: This is the *Old* source-code repository for Proteomics shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sharedpackages/commons3_code/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/proteomics_code/**  

---  
  